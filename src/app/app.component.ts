import { HttpErrorResponse } from '@angular/common/http';
import { ElementRef } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { CheckboxControlValueAccessor } from '@angular/forms';
import { Todo } from './todo';
import { TodoService } from './todo.service';
//@ts-ignore: Object is possibly 'null'.
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public todos!: Todo[];

  constructor(private todoService: TodoService) { }
    ngOnInit(): void {
    this.getTodos();
  }

  public getTodos(): void {

    this.todoService.getTodos().subscribe(
      (response: Todo[]) => {
        this.todos = response;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public setColor(): void {

  }
  //Add an entry via the TextBox to the Todolist
  public onAddButtonClick(item: any): void {
    if (item != null) {
      let newTodo: Todo = new Todo(item.value, false);
      this.todoService.addTodo(newTodo).subscribe(
        (response: Todo) => {
          console.log(response);
          this.getTodos();
        },
        (error: HttpErrorResponse) => {
          alert(error.message);
        }
      );
      item.value = '';
    }
    else {
      console.log("test");
    }
  }

  //Delete all entries of the Todolist
  public deleteList(): void {
    for (let todo of this.todos) {
      console.log("Todo mit ID:" + todo.id + todo.task + todo.isDone + "wurde gelöscht.")
      this.todoService.deleteTodo(todo.id).subscribe(
        (response: void) => {
          console.log(response);
          this.getTodos();
        },
        (error: HttpErrorResponse) => {
          alert(error.message);
        }
      );
    }
  }

  public deleteTask(taskId: number) {
    
    this.todoService.deleteTodo(taskId).subscribe(
      (response: void) => {
        console.log(response);
        this.getTodos()
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }
  
  public setStatusTrue(id: number, taskId: any) {
    // For coloring the Todo
  const taskElement = document.getElementById(taskId);
    if (taskElement == null) alert("test");
    else {
      taskElement.style.color = "green";
      this.todoService.setStatusTrue(id).subscribe(
        (response: Todo) => {
          console.log(response);
         // this.getTodos();
         
        },
        (error: HttpErrorResponse) => {
          alert(error.message);
        }
      );
    }
  }

  public setStatusFalse(id: number, taskId: any) {
    //For coloring The Todo
    const taskElement = document.getElementById(taskId);
    if (taskElement == null) alert("test");
    else {
      taskElement.style.color = "red";
     // taskElement.style.backgroundColor = "red";

      this.todoService.SetStatusFalse(id).subscribe(
        (response: Todo) => {
        },
        (error: HttpErrorResponse) => {
          alert(error.message);
        }
      );
    }
  }
}
