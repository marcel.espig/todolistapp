export interface Todo{
  id: number;
  task: String;
  isDone: boolean;
}


export class Todo implements Todo {
  task: String;
  isDone: boolean;
  constructor(task: String, isDone: boolean) {
    this.task = task;
    this.isDone = isDone;
 
  }
}
